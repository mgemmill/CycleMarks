CycleMarks.vim
==============

simplified mark setting and navigation for vim
----------------------------------------------

Do you find you can't remember what mark you placed
where in your document? 
*Was that at "a" or was it "b"?*

Do you find you can't recall if you already set a mark? 
*Oh god, did I already use "c"?*

Do you find your automatic typing reflexes tend to 
set mark "a"? 
*Goddamnit, I meant "b"!*

It could be a sign your memory is failing. Or perhaps it was never that 
good to begin with.
Well, worry no more.  **CycleMarks** saves you from these 
annoyances by providing simplified mark setting and navigation.

This is how **CycleMarks** works:

1. Set a mark on the current line with <Leader>m. No need to specify a
   register, **CycleMarks** just assigns the mark for you. 

2. **CycleMarks** will cycle through your marks backwards or forwards from
   your current position in the file. **CycleMarks** leaves it up to the user
   to specify the navigation keys.

**CycleMarks** also provides a feature to restrict the range of marks it
uses (defaults to a-z).

**CycleMarks** uses vim's standard mark functionality, so you can still 
navigate marks directly via vim's `'{mark}` command.

Quick Start
-----------

**CycleMarks** provides the following key mapping to set a mark:
    
    <leader>m

**CycleMarks** does not provide any default mappings for cycling through
your marks. so you'll need to add those commands to your vimrc file.

Example:

    nnoremap <Leader>p :CMGoPrevMark
    nnoremap <Leader>n :CMGoNextMark

or maybe you'd prefer:

    nnoremap <Up>   :CMGoPrevMark
    nnoremap <Down> :CMGoNextMark

Now you're ready to go!

Installing CycleMarks
=====================

Pathogen Installation
---------------------

[pathogen](http://www.vim.org/scripts/script.php?script_id=2332)
is the recommended way to install CycleMarks.  

Using pathogen the manual way: 

    cd ~/.vim/bundle 
    git clone https://bitbucket.org/mgemmill/cyclemarks.git 

For the less manual way, 
[vim-update-bundles](https://github.com/bronson/vim-update-bundles/)
is an excellent option.

Manual Installation
-------------------

Extract the zip file into your ~/.vim or $HOME\vimfiles (windows), open up vim, 
run helptags and then checkout :help syntastic.

Grab the latest dev version from 
[bitbucket](https://bitbucket.org/mgemmill/cyclemarks)
